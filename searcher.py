import re
import pypff
import csv
import sys
import click

reload(sys)
sys.setdefaultencoding('utf8')

regexdict = {  # TODO Convert this to an imported option from a config file or accept raw input at the console
    "ssn": r"^(\d{3}-?\d{2}-?\d{4}|XXX-XX-XXXX)$",
    "email": r".+\@.+\..+",
    "cc": r"^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$",
    "BSC": r"^\d{7}$",
    "PhoneNumbers": r"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$",
    "keyword1": r"medical",
    "keyword2": r"invoice",
    "keyword3": r"statement",
    "keyword4": r"merchant",
}

def importpst(pstfile):
    """
    Function to open PST file and return the root folder
    :param pst: PST file
    :return: root folder object
    """
    opst = pypff.open(pstfile)
    root = opst.get_root_folder()
    return root


def masrescon(hittype, subject, sender, deltime, matchdata):
    """
    Format and return a dictionary with message data
    :param hittype: This field represents the regex pattern that was matched
    :param subject: This field represents the subject line of the email
    :param sender: This field represents the sender address
    :param deltime: This field represents the delivery time
    :param matchdata: This field represents the data that contains the regex match
    :return: A dictionary with result data
    """
    return {
        'hittype': hittype,
        'subject': subject,
        'sender': sender,
        'deltime': deltime,
        'matchdata': matchdata,
    }


def process(message):
    """
    Parse the message data and return it as a dictionary
    :param message: A pypff formated message object
    :return: Message data converted into a dictionary
    """
    return {
        "subject": message.subject,
        "sender": message.sender_name,
        "header": message.transport_headers,
        "body": message.plain_text_body,
        "creation_time": message.creation_time,
        "submit_time": message.client_submit_time,
        "delivery_time": message.delivery_time,
        "attachment_count": message.number_of_attachments,
    }


def folderex(base):
    """
    Take the root folder and create a list will all folder objects in the PST, it also
    checks to see if a global 'folderlist' variable exists, and if not creates it
    :param base: A pypff root folder object
    :return:None
    """
    if 'folderlist' in globals():
        pass
    else:
        global folderlist
        folderlist = []

    for folder in base.sub_folders:
        if folder.number_of_sub_folders:
            folderex(folder)
        folderlist.append(folder)


def folistm(folderlist):
    """
    Take a list of PST extracted folder objects and iterate over them extracting all messages and adding them to a global
    message list variable called 'messagelist', if the global variable doesn't exist, create it
    :param folderlist: A list object containing pypff formated folder objects
    :return: None
    """

    if 'messagelist' in globals():
        pass
    else:
        global messagelist
        messagelist = []

    for folder in folderlist:
        for message in folder.sub_messages:
            messagelist.append(process(message))


def regexcompiler(regexdict):
    """
    Convert a dictionary of regex names and raw regex strings into
    a new key pair of name and compiled regex search objects
    :param regexdict: Dictionary with name and raw regex string pairs
    :return: Dictionary with key pairs of a name and compiled regex search object
    """
    for key, value in regexdict.iteritems():
        regexdict[key] = re.compile(value, re.MULTILINE | re.IGNORECASE)
    return regexdict


def search(inputa, regexquery):
    """
    Run the precompiled regex queries and return a dictionary with results if they exist or with a result value of false if they do not
    :param inputa: Data you want to run regex against
    :param regexquery: Compiled regex search object
    :return: Dictionary with boolean true or false based on whether or not there was a match, and if true a copy of the line that matched
    """
    results = {}
    if inputa is not None:
        matches = regexquery.search(inputa)
        if matches != None:
            results['result'] = True
            results['data'] = matches.group()
        else:
            results['result'] = False
        return results
    else:
        results['result'] = False
        return results

def searchmessage(messagelis, regexlist):
    """
    Iterate over the message list and run the regex query against the body and subject lines of each message
    Append results to the masreslist and also print them to the prompt, returns a list of dictionary formated
    results
    :param messagelis: List object containing message data in dictionary format
    :param regexlist: Dictionary with key pairs that are in the format name and compiled regex search object
    :return: A list object containing one or more dictionary objects that contain results
    """
    masreslist = []
    for message in messagelis:
        for key, value in regexlist.iteritems():
            res = search(message['subject'], value)
            if res['result'] == False:
                pass
            else:
                print 'Hit on ' + key + ' Subject:' + message['subject'] + ' Sender:' + message['sender'] + ' Delivery Time:' + unicode(str(message['delivery_time']),errors='replace') + ' Match Data:' + res['data']
                masreslist.append(masrescon(key, message['subject'], message['sender'], message['delivery_time'], res['data']))

            res = search(message['body'], value)
            if res['result'] == False:
                pass
            else:
                print 'Hit on ' + key + ' Subject:' + message['subject'] + ' Sender:' + message['sender'] + ' Delivery Time:' + unicode(str(message['delivery_time']),errors='replace') + ' Match Data:' + res['data']
                masreslist.append(masrescon(key, message['subject'], message['sender'], message['delivery_time'], res['data']))
    return masreslist

def filewriter(masreslist, outfile):
    """
    This function is used to write out a spreadsheet of the results
    :param masreslist: A list of dictionary objects containing the results
    :return: None
    """
    with open(outfile, 'w') as csvfile:
        fieldnames = ['hittype', 'subject', 'sender', 'deltime', 'matchdata']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(masreslist)


@click.command()
@click.option('--pst', '-p', help='PST file you would like to parse')
@click.option('--outfile', '-o', help='File to output the results from')
def main(pst, outfile):
    regexstrings = regexcompiler(regexdict)
    root = importpst(pst)
    folderex(root)
    folistm(folderlist)
    if len(messagelist) > 0:
        print "Processing " + str(len(messagelist)) + " emails"
        resultset = searchmessage(messagelist, regexstrings)
        if len(resultset) > 0:
            print "Number of Hits:" + str(len(resultset))
        else:
            print "No Hits"
    filewriter(resultset, outfile)

if __name__ == "__main__":
    main()
